<section class="sidebar">
    <div id="menu" role="navigation">
        <div class="sidebar-nav">
            <div class="nav_profile">
                <div class="media profile-left">
                    <div class="nav_icons">
                        <ul class="sidebar_threeicons">
                            <li>
                                <a href="/settings">
                                    <i class="livicon" data-name="gears" title="Settings" data-loop="true" data-color="#418BCA" data-hc="#418BCA" data-s="25"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/settings#/security">
                                    <i class="livicon" data-name="lock" title="Security" data-loop="true" data-color="#e9573f" data-hc="#e9573f" data-s="25"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/settings#/api">
                                    <i class="livicon" data-name="list-ul" title="API" data-loop="true" data-color="#F89A14" data-hc="#F89A14" data-s="25"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/logout">
                                    <i class="livicon" data-name="sign-out" title="Logout" data-loop="true" data-color="#6CC66C" data-hc="#6CC66C" data-s="25"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <ul class="navigation" id="navigation">
            <li>
                <a href="/home">
                    <i class="livicon" data-name="home" data-size="18" data-loop="true" data-color="#428bca" data-hc="#428bca"></i>
                    <span class="mm-text ">Home</span>
                </a>
            </li>
            <li class="menu-dropdown">
                <a href="javascript:void(0)">
                    <i class="livicon" data-name="gears" data-size="18" data-loop="true" data-color="#6CC66C" data-hc="#6CC66C"></i>
                    <span>
                                    Settings
                                </span>
                    <span class="fa arrow"></span>
                </a>
                <ul>
                    <!-- Profile Link -->
                    <li>
                        <a href="/settings#/profile" aria-controls="profile">
                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-color="#5bc0de" data-hc="#5bc0de"></i> Profile
                        </a>
                    </li>
                    <!-- Teams Link -->
                    @if (Spark::usesTeams())
                    <li>
                        <a href="/settings#/{{str_plural(Spark::teamsPrefix())}}" aria-controls="teams">
                            <i class="livicon" data-name="users" data-size="18" data-loop="true" data-color="#00bc8c" data-hc="#00bc8c"></i> {{ ucfirst(str_plural(Spark::teamsPrefix())) }}
                        </a>
                    </li>
                    @endif
                    <!-- Security Link -->
                    <li>
                        <a href="/settings#/security" aria-controls="security">
                            <i class="livicon" data-name="lock" data-size="18" data-loop="true" data-color="#F89A14" data-hc="#F89A14"></i> Security
                        </a>
                    </li>
                    <!-- API Link -->
                    @if (Spark::usesApi())
                    <li>
                        <a href="/settings#/api" aria-controls="api">
                            <i class="livicon" data-name="list-ul" data-size="18" data-loop="true" data-color="#EF6F6C" data-hc="#EF6F6C"></i> API
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @if (Spark::canBillCustomers())
            <li class="menu-dropdown">
                <a href="javascript:void(0)">
                    <i class="livicon" data-name="piggybank" data-size="18" data-loop="true" data-color="#6CC66C" data-hc="#6CC66C"></i>
                    <span>
                                    Billing
                                </span>
                    <span class="fa arrow"></span>
                </a>
                <ul>
                    @if (Spark::hasPaidPlans())
                    <!-- Subscription Link -->
                    <li>
                        <a href="/settings#/subscription" aria-controls="subscription">
                            <i class="livicon" data-name="shopping-cart-in" data-size="18" data-loop="true" data-color="#00bc8c" data-hc="#00bc8c"></i> Subscription
                        </a>
                    </li>
                    @endif
                    <!-- Payment Method Link -->
                    <li>
                        <a href="/settings#/payment-method" aria-controls="payment-method">
                            <i class="livicon" data-name="credit-card" data-size="18" data-loop="true" data-color="#EF6F6C" data-hc="#EF6F6C"></i> Payment Method
                        </a>
                    </li>
                    <!-- Invoices Link -->
                    <li>
                        <a href="/settings#/invoices" aria-controls="invoices">
                            <i class="livicon" data-name="table" data-size="18" data-loop="true" data-color="#428bca" data-hc="#428bca"></i> Invoices
                        </a>
                    </li>
                </ul>
            </li>
            @endif @if (Spark::developer(Auth::user()->email))
            <li class="menu-dropdown">
                <a href="javascript:void(0)">
                    <i class="livicon" data-name="user-flag" data-size="18" data-loop="true" data-color="#428bca" data-hc="#428bca"></i>
                    <span>kiosk</span>
                    <span class="fa arrow"></span>
                </a>
                <ul>
                    <!-- Announcements Link -->
                    <li>
                        <a href="/spark/kiosk#/announcements">
                            <i class="livicon" data-name="speaker" data-size="18" data-loop="true" data-color="#ef6f6c" data-hc="#ef6f6c"></i> Announcements
                        </a>
                    </li>
                    <!-- Metrics Link -->
                    <li>
                        <a href="/spark/kiosk#/metrics">
                            <i class="livicon" data-name="barchart" data-size="18" data-loop="true" data-color="#6CC66C" data-hc="#6CC66C"></i> Metrics
                        </a>
                    </li>
                    <!-- Users Link -->
                    <li>
                        <a href="/spark/kiosk#/users">
                            <i class="livicon" data-name="user" data-size="18" data-loop="true" data-color="#5bc0de" data-hc="#5bc0de"></i> Users
                        </a>
                    </li>
                </ul>
            </li>
            @endif @if (Spark::usesTeams())
            <li {!! (Request::is('settings/'.str_plural(Spark::teamsPrefix()).'/*') ? 'class="menu-dropdown active"' : 'class="menu-dropdown"') !!}>
                <a data-toggle="collapse" href="#collapseTeam" href="#"><i class="ti-user"></i><span> {{ ucfirst(str_plural(Spark::teamsPrefix())) }} Settings</span><span class="fa arrow"></span></a>
                <ul {!! (Request::is('settings/'.str_plural(Spark::teamsPrefix()).'/*') ? 'class="collapse show active"' : 'class="collapse"') !!} id="collapseTeam" data-parent="#navigation">
                    <li v-for="team in teams" :class="{active:team.id=={{Request::is('settings/'.str_plural(Spark::teamsPrefix()).'/*')?substr(Request::path(),strlen('settings/'.str_plural(Spark::teamsPrefix()).'/')):0}}}">
                        <a :href="'/settings/{{str_plural(Spark::teamsPrefix())}}/'+team.id">
                            <span>
                <img :src="team.photo_url" class="spark-team-photo-xs"><i class="fa fa-btn"></i><span> @{{ team.name }}</span>
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif @if (Spark::usesTeams() && (Spark::createsAdditionalTeams() || Spark::showsTeamSwitcher()))
            <!-- Team Settings -->
            @include('spark::nav.teams') @endif
        </ul>
        <!-- / .navigation -->
    </div>
    <!-- menu -->
</section>
