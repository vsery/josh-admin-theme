<form class="form-horizontal" role="form" id="reg_form">
    <h3 class="black_bg text-center">
        <img src="/img/logo.png" alt="josh logo">
        <br>
        <span v-if="paidPlans.length > 0">
            Profile
        </span>
        <span v-else>
            Sign Up
        </span>
    </h3>
    @if (Spark::usesTeams() && Spark::onlyTeamPlans())
        <!-- Team Name -->
        <div class="form-group " :class="{'has-error': registerForm.errors.has('team')}" v-if=" ! invitation">


            <label class=" col-form-label text-md-right youteam" style="margin-bottom:0px;" for="team">
                <i class="livicon" data-name="users" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                {{ __('teams.team_name') }}
            </label>



                <input type="text" class="form-control" name="team" v-model="registerForm.team" :class="{'is-invalid': registerForm.errors.has('team')}" autofocus placeholder=" {{ __('teams.team_name') }}" id="team">

                <span class="invalid-feedback help-block" v-show="registerForm.errors.has('team')">
                    @{{ registerForm.errors.get('team') }}
                </span>

        </div>

        @if (Spark::teamsIdentifiedByPath())
            <!-- Team Slug (Only Shown When Using Paths For Teams) -->
            <div class="form-group row" :class="{'has-error': registerForm.errors.has('team_slug')}" v-if=" ! invitation">

                <label class="col-md-4 col-form-label text-md-right">{{ __('teams.team_slug') }}</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="team_slug" v-model="registerForm.team_slug" :class="{'is-invalid': registerForm.errors.has('team_slug')}" autofocus>

                    <small class="form-text text-muted" v-show="! registerForm.errors.has('team_slug')">
                        {{__('teams.slug_input_explanation')}}
                    </small>

                    <span class="invalid-feedback" v-show="registerForm.errors.has('team_slug')">
                        @{{ registerForm.errors.get('team_slug') }}
                    </span>
                </div>
            </div>
        @endif
    @endif

    <!-- Name -->
    <div class="form-group " :class="{'has-error': registerForm.errors.has('name')}">
        <label class=" col-form-label  youmail" style="margin-bottom:0px;" for="name">
            <i class="livicon" data-name="user" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
            {{__('Name')}}
        </label>


            <input type="text" class="form-control" name="name" v-model="registerForm.name" :class="{'is-invalid': registerForm.errors.has('name')}" autofocus placeholder=" {{__('Name')}}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('name')">
                @{{ registerForm.errors.get('name') }}
            </span>

    </div>

    <!-- E-Mail Address -->
    <div class="form-group " :class="{'has-error': registerForm.errors.has('email')}">
        <label class=" col-form-label  youmail" style="margin-bottom:0px;" for="email">
            <i class="livicon" data-name="mail" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
            {{__('E-Mail Address')}}
        </label>


            <input type="email" class="form-control" name="email" v-model="registerForm.email" :class="{'is-invalid': registerForm.errors.has('email')}" placeholder=" {{__('E-Mail Address')}}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('email')">
                @{{ registerForm.errors.get('email') }}
            </span>

    </div>

    <!-- Password -->
    <div class="form-group " :class="{'has-error': registerForm.errors.has('password')}">
        <label class=" col-form-label youpasswd" style="margin-bottom:0px;" for="password" >
            <i class="livicon" data-name="key" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
            {{__('Password')}}
        </label>


            <input type="password" class="form-control" name="password" v-model="registerForm.password" :class="{'is-invalid': registerForm.errors.has('password')}" placeholder="{{__('Password')}}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('password')">
                @{{ registerForm.errors.get('password') }}
            </span>

    </div>

    <!-- Password Confirmation -->
    <div class="form-group " :class="{'has-error': registerForm.errors.has('password_confirmation')}">
        <label class=" col-form-label  youpasswd" style="margin-bottom:0px;" for="password">
            <i class="livicon" data-name="key" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
            {{__('Confirm Password')}}
        </label>


            <input type="password" class="form-control" name="password_confirmation" v-model="registerForm.password_confirmation" :class="{'is-invalid': registerForm.errors.has('password_confirmation')}" placeholder="{{__('Confirm Password')}}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('password_confirmation')">
                @{{ registerForm.errors.get('password_confirmation') }}
            </span>

    </div>

    <!-- Terms And Conditions -->
    <div v-if=" ! selectedPlan || selectedPlan.price == 0">
        <div class="form-group " :class="{'has-error': registerForm.errors.has('terms')}">

                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="terms" name="terms" :class="{'is-invalid': registerForm.errors.has('terms')}" v-model="registerForm.terms">
                    <label class="form-check-label" for="terms">
                        {!! __('I Accept :linkOpen The Terms Of Service :linkClose', ['linkOpen' => '<a href="/terms" target="_blank">', 'linkClose' => '</a>']) !!}
                    </label>
                    <div class="invalid-feedback" v-show="registerForm.errors.has('terms')">
                        <strong>@{{ registerForm.errors.get('terms') }}</strong>
                    </div>
                </div>

        </div>

        <div class="form-group">

                <button class="btn btn-success btn-block" @click.prevent="register" :disabled="registerForm.busy">
                    {{--<span v-if="registerForm.busy">--}}
                        {{--<i class="fa fa-btn fa-spinner fa-spin"></i> {{__('Registering')}}--}}
                    {{--</span>--}}

                    {{--<span v-else>--}}
                        {{--<i class="fa fa-btn fa-check-circle"></i> {{__('Register')}}--}}
                    {{--</span>--}}
                    <span v-if="registerForm.busy">
                        <i class="fa fa-btn fa-spinner fa-spin"></i>Signing Up
                    </span>
                    <span v-else>
                        Sign Up
                    </span>
                </button>

        </div>
        <div class="form-group">
            <div>
                <p class="change_link">
                    <a href="/login" class="to_register">
                        <button type="button" class="btn btn-responsive botton-alignment btn-warning btn-sm">Back</button>
                    </a>
                </p>
            </div>
        </div>

    </div>
</form>
