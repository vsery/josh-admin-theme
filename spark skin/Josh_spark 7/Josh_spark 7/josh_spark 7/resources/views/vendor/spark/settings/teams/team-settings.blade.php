@extends('spark::layouts.app')

@section('scripts')
    @if (Spark::billsUsingStripe())
        <script src="https://js.stripe.com/v3/"></script>
    @else
        <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
    @endif
@endsection

@section('content')
    <spark-team-settings :user="user" :team-id="{{ $team->id }}" inline-template>
        <div class="spark-screen container">
            <div class="row">
                <!-- Tabs -->
                <div class="col-md-6 spark-settings-tabs">

                        <div class="card border-info">
                            <div class="card-header bg-info text-white">
                                <h3 class="nav-heading text-white mb-0 ">
                                    {{__('teams.team_settings')}}
                                </h3>
                            </div>
                            <div class="card-body p-0 teamSettings">
                            <aside>
                            <ul class="nav flex-column ">
                                @if (Auth::user()->ownsTeam($team))
                                    <li class="nav-item ">
                                        <a class="nav-link" href="#owner" aria-controls="owner" role="tab"
                                           data-toggle="tab">
                                            <i class="livicon" data-name="edit" data-size="20" data-c="#a4aaae"
                                               data-hc="#a4aaae" data-loop="true"></i>
                                            <span class="team-left">{{__('teams.team_profile')}}</span>
                                        </a>
                                    </li>
                                @endif

                                <li class="nav-item ">
                                    <a class="nav-link" href="#membership" aria-controls="membership" role="tab"
                                       data-toggle="tab">
                                        <i class="livicon" data-name="users" data-size="20" data-c="#a4aaae"
                                           data-hc="#a4aaae" data-loop="true"></i>
                                        <span class="team-left">{{__('Membership')}}</span>
                                    </a>
                                </li>

                                @if (Spark::createsAdditionalTeams())
                                    <li class="nav-item ">
                                        <a class="nav-link" href="/settings#/{{Spark::teamsPrefix()}}">
                                            <i class="livicon" data-name="angle-double-left" data-size="20"
                                               data-c="#a4aaae" data-hc="#a4aaae" data-loop="true"></i>

                                            <span class="team-left"> {{__('teams.view_all_teams')}}</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item ">
                                        <a class="nav-link" href="/settings">
                                            <svg class="icon-20 " viewBox="0 0 20 20 "
                                                 xmlns="http://www.w3.org/2000/svg ">
                                                <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                            </svg>
                                            <span class="team-left">{{__('Your Settings')}}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                           </aside>
                            </div>
                        </div>

                </div>
                <div class="col-md-6 spark-settings-tabs">

                    @if (Spark::canBillTeams() && Auth::user()->ownsTeam($team))
                        <aside>
                            <div class="card border-success">
                                <div class="card-header bg-success text-white">
                                    <h3 class="nav-heading text-white mb-0 ">
                                        {{__('teams.team_billing')}}
                                    </h3>
                                </div>
                                <ul class="nav flex-column">
                                    @if (Spark::hasPaidTeamPlans())
                                        <li class="nav-item ">
                                            <a class="nav-link" href="#subscription" aria-controls="subscription"
                                               role="tab" data-toggle="tab">
                                                <i class="livicon" data-name="shopping-cart" data-size="20"
                                                   data-c="#a4aaae" data-hc="#a4aaae" data-loop="true"></i>
                                                <span class="team-left">{{__('Subscription')}}</span>
                                            </a>
                                        </li>

                                        <li class="nav-item ">
                                            <a class="nav-link" href="#payment-method" aria-controls="payment-method"
                                               role="tab" data-toggle="tab">
                                                <i class="livicon" data-name="credit-card" data-size="20"
                                                   data-c="#a4aaae" data-hc="#a4aaae" data-loop="true"></i>
                                                <span class="team-left">{{__('Payment Method')}}</span>
                                            </a>
                                        </li>

                                        <li class="nav-item ">
                                            <a class="nav-link" href="#invoices" aria-controls="invoices" role="tab"
                                               data-toggle="tab">
                                                <i class="livicon" data-name="printer" data-size="20" data-c="#a4aaae"
                                                   data-hc="#a4aaae" data-loop="true"></i>
                                                <span class="team-left">{{__('Invoices')}}</span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </aside>
                    @endif
                </div>

                <!-- Tab cards -->
                <div class="col-md-12">
                    <div class="tab-content">
                        <!-- Owner Information -->
                        @if (Auth::user()->ownsTeam($team))
                            <div role="tabcard" class="tab-pane active" id="owner">
                                @include('spark::settings.teams.team-profile')
                            </div>
                        @endif

                    <!-- Membership -->
                        @if (Auth::user()->ownsTeam($team))
                            <div role="tabcard" class="tab-pane" id="membership">
                                @else
                                    <div role="tabcard" class="tab-pane active" id="membership">
                                        @endif
                                        <div v-if="team">
                                            @include('spark::settings.teams.team-membership')
                                        </div>
                                    </div>

                                    <!-- Billing Tab Panes -->
                                    @if (Spark::canBillTeams() && Auth::user()->ownsTeam($team))
                                        @if (Spark::hasPaidTeamPlans())
                                        <!-- Subscription -->
                                            <div role="tabcard" class="tab-pane" id="subscription">
                                                <div v-if="user && team">
                                                    @include('spark::settings.subscription')
                                                </div>
                                            </div>
                                        @endif

                                    <!-- Payment Method -->
                                        <div role="tabcard" class="tab-pane" id="payment-method">
                                            <div v-if="user && team">
                                                @include('spark::settings.payment-method')
                                            </div>
                                        </div>

                                        <!-- Invoices -->
                                        <div role="tabcard" class="tab-pane" id="invoices">
                                            <div v-if="user && team">
                                                @include('spark::settings.invoices')
                                            </div>
                                        </div>
                                    @endif
                            </div>
                    </div>
                </div>
            </div>
    </spark-team-settings>
@endsection
