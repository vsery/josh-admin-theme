@extends('spark::layouts.layout_2')
@section("styles")
    <link rel="stylesheet" type="text/css" href="/css/login.css">
@endsection

<!-- Main Content -->
@section('content')
<div class="container email-page">
    <div class="row justify-content-center">
        <div class="col-lg-4">

            <div id="container_demo">
                <div id="wrapper">
            <div class="card card-default">
{{--                <div class="card-header">{{__('Reset Password')}}</div>--}}

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form role="form" method="POST" action="{{ url('/password/email') }}">
                        <h3 class="black_bg"><img src="/img/logo.png" alt="josh logo"><br>Forgot Password</h3>
                        <p>
                            Enter your email address below and we'll send a special reset password link to your inbox.
                        </p>
                        {!! csrf_field() !!}

                        <!-- E-Mail Address -->
                        <div class="form-group {{ $errors->has('email') ? ' is-invalid' : '' }}">
                            <label class="col-12 col-form-label youmai" style="margin-bottom:0px;" for="emailsignup1">
                                <i class="livicon" data-name="mail" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                                {{__('E-Mail Address')}}</label>

                            <div class="col-md-12">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="{{__('E-Mail Address')}}">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Send Password Reset Link Button -->
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<p class="login button">--}}
                                    {{--<input type="submit" value="Reset Password" class="btn btn-success" />--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        <div class="form-group">
                            <div class="col-md-12">
                                {{--<button type="submit" class="btn btn-success">--}}
                                    {{--Reset Password--}}
                                {{--</button>--}}
                                <p class="login button">
                                    <input type="submit" value="Reset Password" class="btn btn-success" />
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <p class="change_link">
                                    <a href="/login" class="to_register">
                                        <button type="button" class="btn btn-responsive botton-alignment btn-warning btn-sm">Back</button>
                                    </a>
                                </p>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
