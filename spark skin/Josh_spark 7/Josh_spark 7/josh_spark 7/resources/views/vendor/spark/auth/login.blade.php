@extends('spark::layouts.layout_2')

@section("styles")
    <link href="/css/themify-icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/login.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5  col-sm-12 col-lg-4 login-form animate scaleIn mx-auto">

            <div id="container_demo">
                <div id="wrapper">
                    <div id="login" class="form pt-4">

                    <form class="form-horizontal omb_loginForm" role="form" method="POST" action="/login" autocomplete="off">
                        <h3 class="black_bg"><img src="/img/logo.png" alt="josh logo" /><br />Log In</h3>
                    @include('spark::shared.errors') {{ csrf_field() }}

                    {{ csrf_field() }}

                        <!-- E-Mail Address -->
                        <div class="form-group ">
                            <label style="margin-bottom:0px;" for="email"class="col-form-label uname">
                                <i class="livicon" data-name="mail" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                                {{__('E-Mail')}}
                            </label>


                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="{{__('E-Mail')}}">

                        </div>

                        <!-- Password -->
                        <div class="form-group ">
                            <label style="margin-bottom:0px;" for="password"  class=" col-form-label youpasswd">
                                <i class="livicon" data-name="key" data-size="17" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                                {{__('Password')}}</label>


                                <input type="password" class="form-control" name="password" placeholder="Password">

                        </div>

                        <!-- Remember Me -->
                        <div class="form-group ">

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="remember" class="form-check-input">  Keep me logged in
                                    </label>
                                </div>

                        </div>

                        <!-- Login Button -->
                        <div class="form-group row">
                            <div class="col-md-12">
                                <p class="login button">
                                    <input type="submit" value="Log In" class="btn btn-success" />
                                </p>
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{__('Login')}}--}}
                                {{--</button>--}}

                                {{--<a class="btn btn-link" href="{{ url('/password/reset') }}">{{__('Forgot Your Password?')}}</a>--}}
                            </div>
                        </div>
                        <p class="change_link">
                            <a href="/password/reset">
                                <button type="button" class="btn btn-responsive botton-alignment btn-warning btn-sm">Forgot password</button>
                            </a>
                            <a href="/register">
                                <button type="button" id="signup" class="btn btn-responsive botton-alignment btn-success btn-sm" style="float:right;">Sign Up</button>
                            </a>
                        </p>
                    </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
