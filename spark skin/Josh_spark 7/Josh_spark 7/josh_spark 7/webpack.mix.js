let mix = require('laravel-mix');
let exec = require('child_process').exec;
let path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



//source paths
var resourcesAssets = 'resources/';
var srcCss = resourcesAssets + 'css/';
var nodes = 'node_modules/'

//====== destination path configuration ======

var dest = 'public/';
var destFonts = dest + 'fonts/';
var destCss = dest + 'css/';
var destJs = dest+'js/';
var destVendors = dest + 'vendors/';

// =====paths for vendors
var paths = {

    'raphael':nodes + 'raphael/'
};

// ========copy images=====
mix.copy(resourcesAssets + 'img', 'public/img');

// ========copy fonts=====
mix.copy(resourcesAssets + 'fonts', 'public/fonts');

// ========copy css=====
mix.copy(resourcesAssets + 'css', "public/css");

// ========  js =======
mix.copy(paths.raphael+ 'raphael.min.js', destJs + 'raphael/js');
// =======mix styles======
mix.sass('resources/sass/bootstrap/bootstrap.scss', 'public/css');
mix.sass(resourcesAssets + 'sass/custom.scss', 'public/css');
mix.js('resources/js/livicons-1.4.min.js', 'public/js');





mix
    .sass('resources/sass/app.scss', 'public/css')
    .js('resources/js/app.js', 'public/js')
    .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
    .sass('resources/sass/app-rtl.scss', 'public/css')
    .then(() => {
        exec('node_modules/rtlcss/bin/rtlcss.js public/css/app-rtl.css ./public/css/app-rtl.css');
    })
    .version(['public/css/app-rtl.css'])
    .webpackConfig({
        resolve: {
            modules: [
                path.resolve(__dirname, 'vendor/laravel/spark-aurelius/resources/assets/js'),
                'node_modules'
            ],
            alias: {
                'vue$': mix.inProduction() ? 'vue/dist/vue.min' : 'vue/dist/vue.js'
            }
        }
    });
